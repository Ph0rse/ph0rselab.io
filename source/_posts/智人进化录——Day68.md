---
title: 智人进化录——Day68
date: 2020-05-22 06:40:50
tags: Diary
--- 
## 计划
### 重要且紧急：
1. 英语
    今天要学啥？可以中断几天吗？
    学习技术相关词汇，在2项过程中学习！今天要是能把重要且紧急的任务搞得差不多，那就爽了
    没有怎么看英语诶
3. java审计
    1. 开始规则编写，首先是漏洞规则，然后才是开发
    感觉差不多可以了
    2. RMI和Shiro的内容，前者学习一下，后者写稿。前者还没有看完。  
临时添加
4. 论文修改
down
5. 培训沟通，尽量把实战部分完成。
down，只做靠谱的事情，这是真理。
6. 实习报告
down
7. 参加辩论赛（大学最后一场辩论了）

### 紧急不重要
1. 毕设编程
    1. debug
    2. vue + spring开发
2. 午觉25分钟、11.00准备睡觉，争取明早6.00起床
早上6.25（自然醒，晚上冲个澡，睡眠质量确实不一样，回到理想的入睡状态了），中午15+1（感觉还算舒服，或许是体制提升的结果？），晚上11.50（静心就是快速睡眠）
3. 35分钟慢跑，12分钟腿部（多次）+腹肌+脸颈背训练+练声，还剩8次惩罚（x10波比）
这周还剩3天，把8次惩罚消掉吧。601之后做这件事情，会不会能够警示自己？貌似不用警醒，就已经回到状态了
仍然只做了慢跑
4. 问下取车的事情，准备好今天下午去理发
doing，下午开车去理发。争取3点去，4点回来。超了接近一个钟，高估了自己写论文的速度。



### 重要不紧急
1. NISP二级
2. Java开发手册7天训练【代表开发技能的锻炼】
3. DevSecOps课程+云课程
4. SRS课程
5. 阿里云、Azure、GPG、Amazone学习+认证
6. 腾讯安全应急响应中心文章，主要是lake做企业防御的心得
7. 刷完《数据安全》这本书，内化知识点
继续看了一部分，感觉至少这周可以完成吧。深入技术细节之后，相信收获会很大。
8. 上海交大的安全类学术论文，以及微软那边的技术学术学习建议【收藏夹组织中有录屏】。
9. 建立对前沿技术的时事追踪，漏洞情报（chy）、文章、赏金、时事、科研。
10. 密码学恶补
11. 研究601事件的规律
doing，写到印象里，有比较大的进展
12. 英语-长难句
13. 测试类课程
    1. 慕课，Java、python、Android一把梭
14. Linux运维课程
    1. Shell
    2. sed,awk
15. cnbird2008的云安全研究+议题
16. 陈平的跨学科思维课
17. 观视频中美元霸权等
18. 复习前天的语法和视频，还有数字和日期
这件事情上点心，没有完成


### 不紧急不重要
1. 恶意代码分析技术了解
    把有用的内容消化一下，然后准备放下
    1. FackNet
    doing，尽量
    2. C2_CS索-谢-Red
    3. Cukoo沙盒
    4. Flare VM
    5. 反调试、沙盒、虚拟机API编程
    6. Practicel那套课程搞定，内化为培训材料
    7. Github上的开源资源，再浏览
2. 601
down

## 反省
1. 做得是实事，但没有充分符合原本的计划

## 总结
今天有个好的开头，不再胆怯一些事情了，勇敢滴向前冲！大明王朝可以作为自己业余的消遣，和《朱镕基答记者问》一样
整体打7分，比较勇敢，坚持这种勇敢！大明王朝1个钟，扣2分。参与辩论赛，算是个临时决定，虽然自己感觉很值，但，不应该鼓励这种临时决定，容易坏了原本的计划。因此，扣3分。整体还剩9分。

新增待办：
《得到》上的课程，考虑一下那个游戏设计
计划一下未来几天的出行