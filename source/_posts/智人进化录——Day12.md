---
title: 智人进化录——Day12
date: 2020-03-26 23:36:14
tags: Diary
---


## 昨日总结
专心写稿的感觉还不错，打字速度也上来了。啥时候英文听说写能力上来之后，就腾飞了。给自己打8分，留一个成长类的奖励，尽量周末用了。这个写稿强度，周末应该能完成任务。

### 计划完成情况
固定板块：
1. 午觉25分钟、11.00准备睡觉，争取明早6.30起床
    11.30睡的觉，第二天6.40自然醒了。猜测是运动的效果起来了。继续坚持早睡早起战略吧。
2. 快走+下肢减脂39分钟+热身2分钟+拉伸9分钟+腿部训练12分钟+100仰卧起坐+50俯卧撑
    down
3. 整理身边环境，让生活变得简约
    1. 打字
        速度比之前略快，准确率提高了不少
    2. 归纳（电脑+房间）
        做了一点工作，但提升有限。主要是长期搞乱的环境一时半会儿恢复不来。这周要把这个搞定，并规定以后收纳的规则，提升效率。

新任务：
1. 写书任务（Spring+JMX+现有资讯消化）
    基本完成，学到了不少东西
2. Blackhat, 之前的议题复盘总结 + 新内容Automated Discovery of Deserialization Gadget Chains
    没怎么做，今天的困难部分，就搞这个了。再加上昨天那个RMI绕过。
3. 普通漏洞代码计划编写
    没怎么做，大概有几个模仿对象。

### 反省
1. 娱乐消息看得有点太多了，减少碎片时间上的消耗。
2. 空想不是很符合实际情况，尝试一下把空想的方向，放到书上面。想出更多合理的比喻。
3. 云原生和IOT课程很吸引人，但应该先把基础打扎实，把极客时间和开课吧的课程消化一下之后，再仔细学习云的内容。不晚。
4. 时间的争夺是很残酷的，要采取合适的策略。

### 探索结果
能维持，坚持到周四的时候，状态就比较满意了。接下来继续做实验，坚持之前对自己的要求，然后尝试克服一下周末放松之后的副作用。

## 今日计划
固定板块：
1. 午觉25分钟、11.00准备睡觉，争取明早6.30起床
2. 快走+上肢减脂39分钟+热身2分钟+拉伸9分钟+腿部训练12分钟+100仰卧起坐+50俯卧撑
3. 整理身边环境，让生活变得简约
    1. 打字
    2. 归纳（电脑+房间）

新任务：
1. 写书任务-消化手上的资源（太多了容易崩）、花时间写基础部分
    1. 文章
    2. 议题
    3. 极客时间
    4. 开课吧
    （书的优势也应该体现在，架构全局性，以及复杂设计模式的通俗讲解）
2. Blackhat, 之前的议题复盘总结 + 新内容Automated Discovery of Deserialization Gadget Chains + RMI 绕过
3. 普通漏洞代码计划编写
4. 601事件、602事件（提前做好准备）
5. 平板配环境


今日探索：尝试把放松的副作用降到最低