---
title: 智人进化录——Day19
date: 2020-04-03 08:23:38
tags: Diary
---
## 计划
固定板块：
1. 午觉25分钟、11.00准备睡觉，争取明早6.30起床
    早上自然醒了，舒服~中午时间也控制得比较好，晚上11点就开始泡脚了，完美！
2. 减脂35分钟慢跑、12分钟腿部、塑性、30俯卧撑 * 2（上肢强化）、还剩19次惩罚性腹肌训练。
    俯卧撑和塑性没做，明天尽量把“课件休息”从刷群、刷手机，换到锻炼上。

3. 整理身边环境，让生活变得简约
    1. 归纳（电脑+房间）
        把小米两件产品的原装盒放起来了，之后可能用到。
    2. 减少碎片化时间的浪费，晚上看下基金市场和疫情发展就好，坚持10分钟惩罚制
        控制得比较好，其实就吃饭的时候瞅一眼，应该也行。

擦上周屁股：
1. 议题沉淀（写文章和复现）
2. 根据目录，定向搜索，写稿子（灵活扩展），今天结项
3. 极客时间（每周学习6小时以上，答三道题，就能再领一课，wow），《设计模式》、《深入理解Java虚拟机》，《深入理解Java虚拟机》、两本《Spritn》提取对写书有帮助的内容。
    继续把Java基础写下来，漏洞部分快速冲

## 反省
1. 任务制定得不合理，跟实际的任务方向脱钩。“细化”与“方向正确”之间，后者更重要。
2. 早上刷KEEP时间有点久，不如思考一下目录怎么优化。
3. 其实可以让顺丰把快递放清华园超市，统一去拿的，这样效率高一点。短时间内也确实永不到台灯。
4. 刷群消息的时间也有点久
5. 抽油烟机不行，以后炒完菜洗把脸。
6. 晚上的时间比早上的便宜，这个买卖以后得长做。

## 总结
今天整体打8分！尤其是晚上，心流状态，开始野起来的时候，人挡杀人、佛挡杀佛，这种写作速度，如果能坚持一段时间的话，那对整个人的提升是很恐怖的。指挥官命令，眼前只有一个任务！
虽然1/3任务都没咋开始做，但2部分效果很好，也学到了很多东西。忽然感觉，学做饭挺好的。下周多跟老妈学两招。由于Day19的高效，给自己留一个奖励。

新增待办：
1. 下周跟老妈学做面食和炒菜
2. 俄美、沙特-中，重新理解
3. 废止卖淫嫖娼人员收容教育办法带来的后果讨论【以及中央最近的一些政策调整】
4. 阿里的几次公开课，尤其涉及部门发展方向的讨论和演讲。补一下~
