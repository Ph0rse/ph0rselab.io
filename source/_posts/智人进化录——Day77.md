---
title: 智人进化录——Day77
date: 2020-05-31 07:03:33
tags: Diary
---
## 计划
### 重要且紧急：
1. java审计
    1. 开始规则编写，首先是漏洞规则，然后才是开发
    感觉差不多可以了
    2. RMI和Shiro的内容，前者学习一下，后者写稿。前者还没有看完。  
    3. FastJson、jackson、xstream这些
    看了文章，但整体进度不及预期
    4. 分布式通讯协议文章编写
    今天最重要的决定
2. 讨论辩题
    1. 完成比赛，11点-12点，下午4.00-5.00
    down
3. 租房
    1. 远程看下房，看能不能决定了
    down

### 紧急不重要
1. 毕设
    1. debug
    2. vue + spring开发
    3. 规则编写
    4. PPT
2. 午觉25分钟、11.00准备睡觉，争取明早6.00起床
早上6.20（身体疲惫是快速入眠的关键），中午18分钟（睡晚了，质量不好），晚上0.50（事情处理得没效率）
3. 10公里-down，俯卧撑30，12分钟腿部（多次）+腹肌+脸颈背训练+练声，还剩99波比跳
只完成了10公里，但体重下降得也蛮惊人的，已经到120了

### 重要不紧急
1. NISP二级
2. Java开发手册7天训练【代表开发技能的锻炼】
3. DevSecOps课程+云课程
4. SRS课程
5. 阿里云、Azure、GPG、Amazone学习+认证
6. 腾讯安全应急响应中心文章，主要是lake做企业防御的心得
7. 英语，以技术方面的英语为主
8. 上海交大的安全类学术论文，以及微软那边的技术学术学习建议【收藏夹组织中有录屏】。
9. 建立对前沿技术的时事追踪，漏洞情报（chy）、文章、赏金、时事、科研。
10. 密码学恶补
11. 研究601事件的规律
doing，写到印象里，有比较大的进展
12. 英语-长难句
13. 测试类课程
    1. 慕课，Java、python、Android一把梭
14. Linux运维课程
    1. Shell
    2. sed,awk
15. cnbird2008的云安全研究+议题
16. 陈平的跨学科思维课
17. 观视频中美元霸权等
18. 《得到》剩余课程刷完，并考虑学点关系处理
19. 通过G的报告，来看各大云安全的差异性
![](https://ph0rse.oss-cn-beijing.aliyuncs.com/mmexport1590750183877.jpeg)

### 不紧急不重要
1. 恶意代码分析技术了解
    把有用的内容消化一下，然后准备放下
    1. FackNet
    2. C2_CS索-谢-Red
    3. Cukoo沙盒
    4. Flare VM
    5. 反调试、沙盒、虚拟机API编程
    6. Practicel那套课程搞定，内化为培训材料
    7. Github上的开源资源，再浏览
    8. 以上的内容，写几篇文章出来

## 反省
1. 早上还是想逃避，有半小时的碎片时间。
2. 买汉堡和饮料是个错误的决定，不健康
3. 下午吃得有点多，晚上也多吃了西瓜
4. 辩论的时候有点暴躁
5. 重要的事情，确定办完了之后再离手
6. 坚定地缓慢推进，一个长的时间周期，事半功倍！就像跑步一样

## 总结
整体不如人意，有很多教训，整体打4分吧，加两次惩罚，主要是针对时间掌控、情绪管理、拖延等问题进行处罚。还剩119次波比跳，吃饭没有纳入计划，扣4分，还剩11分。