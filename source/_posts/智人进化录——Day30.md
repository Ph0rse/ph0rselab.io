---
title: 智人进化录——Day30
date: 2020-04-14 09:15:07
tags: Diary
---

## 计划
固定板块：
1. 午觉25分钟、11.00准备睡觉，争取明早6.30起床
早上6.45起床，但赖了15分钟，要改正，或者说要恢复。中午午休用了80分钟，起来的时候感觉都不一样了。晚上少吃点，少喝点（不喝酸奶）争取11点睡觉。
做饭的地点、环境、工具，也会影响结果。
晚上差不多到12.10分才准备睡，然后还睡不着，真的失败……作息养成功亏一篑。今天中午还是28分钟，倒逼晚上早睡。

2. 35分钟燃脂跑+跑后拉伸+12分钟腿部、还剩19次惩罚性腹肌训练
上午、下午、晚上各用一次腿部来代替惩罚性吧。然后再做一次马甲线，今天就能消4次了。用锻炼时间来代替发呆时间（不知道好不好）。
没做额外运动，惩罚性训练还剩19次
3. 整理身边环境，让生活变得简约
    1. 归纳（电脑+房间）【每天11:00之后吧，把心情平复下来】
    2. 减少碎片化时间的浪费，晚上看下基金市场和疫情发展就好，坚持10分钟惩罚制
    3. 练习打字，提升速度（暂停打字计划，放到心累的时候，找一个完整的时间，练习打字）


核心待办：
1. 继续写稿子（细分一下任务，至少完成4个）：
    1. 动态扩展【次要任务】
        1. JNI
        2. OGNL表达式
        3. EL表达式 
        4. （新增）invoke方法原理 - 转为JVM知识，摘取安全相关的内容出来。比如这次的MethodHandle注入类。
    2. 容器安全
        中间件源码、架构
    3. 服务安全
    4. 反序列化
        N个议题，有的是任务
    5. 自动化审计【主要任务，但感觉这一块儿需要再深入一下JVM，跟毕设是同步的】
        1. 审计平台搭建，搞成risp那种的。SpringBoot+Vue
        doing，详见毕设平台
        2. Gadgetinspector架构分析
        明早
        3. CodeQL分析
        doing，今天继续消化，争取效率比昨天高，冲个9分。
        4. Fortify分析
        5. 审计模块设计，学习UML
2. 毕设平台功能编写【主要任务】
    1. 大文件分片上传（记录一下不同服务架构对文件上传漏洞的敏感度）
    2. Vue.js前端
        初步感觉效率还行，根据文档，抓紧把登录、上传、管理、浏览界面搞定
    3. 后台Codeql-cli查询逻辑（要等自动化组件们研究透了之后）
    4. Gadgetinspector架构分析，集成
        今天的主要任务
    5. Fastjson Fuzz架构分析，集成
    6. 消化Fortify的规则，看能不能写成codeql
    7. redis    
        docker redis，快速跑起来


当日任务：
0. 解决一下网卡和VSCode（重装）的问题
网卡就是系统中断导致的，VSCode重装是没有用的，怀疑是有个什么权限配置在作祟，导致修改无效，在虚拟机里的Win10 VSCode是没问题的。接下来有两种可能性，第一，是因为Win10更新，第二，是因为不知名系统原因。后者可以通过系统重装来解决，系统重装估计要一天的时间。
先这样用着，要是今天能把这些任务解决，那可以试着重装一下。做下运动，然后开始冲！

后面有时间的话，搞下VSCode结构，看看这个开源编辑器是怎么构建的，尝试理解一下为什么会出现今天这样的问题。不过找到问题来源的可能性真的不大了

1. 断点上传功能
2. Mybatis+Redis写登录功能
3. Vue.js写前端
4. Codeql文章消化
5. Fortify规则消化

（感觉还是有点多，但心中的预期比这个目标要大得多）
## 反思
1. 不要在早上死磕一个不重要的配置问题，要磕也要晚上磕，最好是时间没那么紧的晚上。

## 总结
今天体会到了破罐子破摔是啥意思……中午睡了将近一个半钟，起来看基金、新闻，看到了6点。到了晚上，潜意识应该是觉得，反正也完不成任务了，就在逃避任务。结果就是，一整天，就完成了0号任务。emmmm
总体打1分……惩罚自己两天半不能有任何娱乐，包括早上的知乎消遣。严格按照计划执行。昨天已经透支了娱乐时间……就当是一次性把未来两三天的娱乐用完了。
尝试一下把未来一段时间的节奏定到无娱乐的紧张状态。吃饭时间，闲散沟通，或构思英文语句（印象），或项目、书稿相关。

新增待办：
1. “本地生活”，美团 vs 阿里本地生活（盒马、口碑、）PS：阿里最近股价上涨是不是跟这个有关？
2. 租房市场，差不多5月份开始，就要开始关注租房动态了。提前一个月，找到好房源，提前问一下谢和校招客服。要达到综合最好；
3. 研究一下，央行定向降准降息，务必会带来通货膨胀，但应该是可控的通货膨胀。积极关注四月份CPI，有空儿了出篇文章。
4. 罗翔的法律课
5. 定制化幽默范式
